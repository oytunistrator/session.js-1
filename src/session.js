var Session = function(file){
    return {
        file: file,
        cookies: function(){
            var pairs = document.cookie.split(";");
            var cookies = {};
            for (var i=0; i<pairs.length; i++){
                var pair = pairs[i].split("=");
                cookies[pair[0]] = unescape(pair[1]);
            }
            return cookies;
        },
        get: function(key){
            var sessionData;

            sessionData = self.ajax(this.file, {
                key: key,  
                id: self.cookies.PHPSESSID
            });

            return sessionData;
        },
        ajax: function(file, item){
            var xhttp;
            var result;
            if (window.XMLHttpRequest) {
                xhttp = new XMLHttpRequest();
            } else {
                xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }

            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    result = JSON.parse(this.responseText);
                }
            };
            xhttp.open("POST", file, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var form_data = new FormData();

            for ( var key in item ) {
                form_data.append(key, item.key);
            }
            xhttp.send(form_data);
            return result.result;
        };
    };
};